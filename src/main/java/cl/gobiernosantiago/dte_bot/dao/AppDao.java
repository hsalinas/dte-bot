package cl.gobiernosantiago.dte_bot.dao;

import java.util.List;

public interface AppDao<T> {

    public List<T> findAll();

    public boolean executeSQL(String hql);

}
