package cl.gobiernosantiago.dte_bot.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TransactionRequiredException;


public abstract class AbstractDaoImpl<T> implements AppDao<T> {

    @Inject
    private EntityManager entityManager;

    private final Class<T> entityClass;

    private static final Logger LOG = Logger.getLogger(AbstractDaoImpl.class.getName());

    public AbstractDaoImpl(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public abstract List<T> findAll();

    public boolean create(T entity) {

        try {

            this.startOperation();

            this.entityManager.persist(entity);

            this.flushAndCommitOperation();

        } catch (IllegalArgumentException | EntityNotFoundException | TransactionRequiredException | IllegalStateException ex) {

            this.rollbackOperation(this.getClass().getSimpleName(), Thread.currentThread().getStackTrace()[1].getMethodName(), ex);

            return false;

        }

        return true;

    }

    public boolean update(T entity) {

        try {

            this.startOperation();

            this.entityManager.merge(entity);

            this.flushAndCommitOperation();

        } catch (IllegalArgumentException | EntityNotFoundException | TransactionRequiredException | IllegalStateException ex) {

            this.rollbackOperation(this.getClass().getSimpleName(), Thread.currentThread().getStackTrace()[1].getMethodName(), ex);

            return false;
        }

        return true;

    }

    public boolean delete(T entity) {

        try {

            this.startOperation();

            this.entityManager.remove(this.entityManager.contains(entity) ? entity : this.entityManager.merge(entity));

            this.flushAndCommitOperation();

        } catch (IllegalArgumentException | EntityNotFoundException | TransactionRequiredException | IllegalStateException ex) {

            this.rollbackOperation(this.getClass().getSimpleName(), Thread.currentThread().getStackTrace()[1].getMethodName(), ex);

            return false;

        }

        return true;
    }

    public T find(Object id) {

        Object obj = null;

        try {

            this.startOperation();

            obj = this.entityManager.find(entityClass, id);

            this.flushAndCommitOperation();

        } catch (IllegalArgumentException | EntityNotFoundException | TransactionRequiredException | IllegalStateException ex) {

            this.rollbackOperation(this.getClass().getSimpleName(), Thread.currentThread().getStackTrace()[1].getMethodName(), ex);

        }

        return this.entityClass.cast(obj);

    }

    @Override
    public boolean executeSQL(String hql) {

        try {

            this.startOperation();

            this.entityManager.createNativeQuery(hql).executeUpdate();

            this.flushAndCommitOperation();

        } catch (Exception ex) {

            this.rollbackOperation(this.getClass().getSimpleName(), Thread.currentThread().getStackTrace()[1].getMethodName(), ex);

            return false;
        }

        return true;

    }

    public void startOperation() {

        this.entityManager.clear();

        this.entityManager.getTransaction().begin();

    }

    public void flushAndCommitOperation() {

        this.entityManager.getTransaction().commit();

    }

    public void rollbackOperation(String nombreClase, String nombreMethod, Exception ex) {

        this.entityManager.getTransaction().rollback();

        LOG.log(Level.INFO, "|| Error Transaction || Class => {0} || Method =>  {1} || Message => {2}", new Object[]{nombreClase, nombreMethod, ex.getMessage()});

    }

    public void stopOperation() {

          // CDI SE ENCARGA DE CERRAR
//        if (this.entityManager != null && this.entityManager.isOpen()) {
//            this.entityManager.close();
//        }
    }

}
