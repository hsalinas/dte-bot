package cl.gobiernosantiago.dte_bot.dao;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;

@ApplicationScoped
@Named("producerResources")
public class ProducerResources {

    @PostConstruct
    public void init() {
        System.out.println(" soy ProducerResources ");
    }

    @Produces    
    private EntityManager beanEntityManager() {
        return EntityManagerUtil.getEntityManagerFactory().createEntityManager();
    }

    public void close(@Disposes EntityManager entityManager) {

        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }

}
