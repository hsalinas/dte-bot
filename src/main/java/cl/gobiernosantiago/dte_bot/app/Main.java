package cl.gobiernosantiago.dte_bot.app;

import cl.gobiernosantiago.dte_bot.controller.ApplicationController;
import java.io.IOException;
import java.net.URISyntaxException;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class Main {

    public static void main(String[] args) throws URISyntaxException, InterruptedException, IOException {

//        Main app = new Main();
//
//        app.setup();
//
//        app.registerUser();
//
//        app.close();
        Weld weld = new Weld();
        WeldContainer weldContainer = weld.initialize();
        weldContainer.select(ApplicationController.class).get().run();
        //weldContainer.select(ApplicationController.class).get().save();

        weldContainer.shutdown();
        
        System.exit(0);

    }

}
