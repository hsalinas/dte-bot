package cl.gobiernosantiago.dte_bot.controller;

import cl.gobiernosantiago.dte_bot.dao.AbstractDaoImpl;
import cl.gobiernosantiago.dte_bot.entity.AceptaReporte;
import java.io.File;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped
@Named("aceptaReporteController")
public class AceptaReporteController extends AbstractDaoImpl<AceptaReporte> {

    public AceptaReporteController() {
        super(AceptaReporte.class);
    }

    @Override
    public List<AceptaReporte> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean create(File fileFinal) {

        AceptaReporte aceptaReporte = new AceptaReporte();

        Date hoy = new Date();

        aceptaReporte.setCreated(hoy);

        aceptaReporte.setModified(hoy);

        aceptaReporte.setName(fileFinal.getName());

        aceptaReporte.setSize((int) fileFinal.length());

        aceptaReporte.setType("application/vnd.ms-excel");

        aceptaReporte.setPath(fileFinal.getName());

        aceptaReporte.setEnabled(true);

        return super.create(aceptaReporte);
    }

}
