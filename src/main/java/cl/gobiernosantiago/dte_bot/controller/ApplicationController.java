package cl.gobiernosantiago.dte_bot.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.inject.Inject;
import net.lingala.zip4j.ZipFile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class ApplicationController {

    @Inject
    private AceptaReporteController aceptaReporteController;

    private WebDriver driver;

    private Wait<WebDriver> fwait;

    private static final int BUFFER_SIZE = 4096;

    //private String downloadFilePath = "/home/hsalinas/Downloads";
    private final String downloadFilePath = "/opt/sga/upload";

    //private final By ingresarBtnLocator = By.xpath("//input[@type='submit' and @value='Ingresar']");
    private final By ingresarBtnLocator = By.xpath("//button[@class='g-recaptcha btn btn-s btn-acepta btn-lg btn-block']");

    private final By rutLocator = By.name("LoginForm[rut]");

    private final By passwordLocator = By.name("LoginForm[password]");

    private final By buscarBtnLocator = By.xpath("//input[@name='buscar2']");

    //Ventana nueva de aviso 20250117
    private final By avisoCerrarBtnLocator = By.xpath("//button[@id='btn_close_aviso_pantalla']");

    private final By fechaInicioLocator = By.name("FSTART");

    private final By exportarBtnLocator = By.xpath("//input[@name='Exportar']");

    private final By reportesLinkLocator = By.linkText("Reportes");

    private final By descargarLinkLocator = By.xpath("//a[@id='1Descargargrilla_reportesNEW']");

    public void setup() {

        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");

        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popup", 0);
        chromePrefs.put("download.default_directory", downloadFilePath);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        this.driver = new ChromeDriver(options);

        //System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
        //driver = new FirefoxDriver();
        this.driver.manage().window().maximize();

        this.driver.get("https://dte.dipres.gob.cl/");

        //Implicit Wait
        //this.driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        //FluentWait
        fwait = new FluentWait<>(this.driver)
                .withTimeout(Duration.ofSeconds(180))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(Exception.class);

    }

    public void registerUser() throws InterruptedException, IOException {

        this.driver.findElement(this.rutLocator).clear();

        //this.driver.findElement(this.rutLocator).sendKeys(new CharSequence[]{"14.911.053-4"});
        this.driver.findElement(this.rutLocator).sendKeys(new CharSequence[]{"13.076.194-1"});

        this.driver.findElement(this.passwordLocator).clear();

        //this.driver.findElement(this.passwordLocator).sendKeys(new CharSequence[]{"Gore.7670!"});
        this.driver.findElement(this.passwordLocator).sendKeys(new CharSequence[]{"Bandera.46"});

        WebElement btnIngresarBtnLocator = fwait.until((WebDriver driver11) -> driver11.findElement(ingresarBtnLocator));

        if (btnIngresarBtnLocator.isEnabled()) {

            btnIngresarBtnLocator.click();

        } else {

            System.out.println("no existe");
        }

        
        
        //Ventana nueva de aviso 20250117
        //fin ventana de aviso 20250205
        /*
        WebElement btnAvisoCerrar = fwait.until((WebDriver driver1) -> driver1.findElement(avisoCerrarBtnLocator));

        if (btnAvisoCerrar.isEnabled()) {

            btnAvisoCerrar.click();

        } else {

            System.out.println("no existe");
        }
        */
        
        //this.driver.findElement(ingresarBtnLocator).click();
        //Thread.sleep(2000);
        WebElement btnBuscar = fwait.until((WebDriver driver1) -> driver1.findElement(buscarBtnLocator));

        //if (this.driver.findElement(buscarBtnLocator).isDisplayed()) {
        if (btnBuscar.isEnabled()) {

            this.documentosRecibidos();

        } else {

            System.out.println("no existe");
        }

    }

    public void documentosRecibidos() throws IOException {

        DateFormat fechaFormat = new SimpleDateFormat("MMddyyyy");

        Calendar cal = Calendar.getInstance();

        Calendar calendar = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 1);

        this.driver.findElement(fechaInicioLocator).clear();

        //this.driver.findElement(fechaInicioLocator).sendKeys(new CharSequence[]{"01072023"});
        this.driver.findElement(fechaInicioLocator).sendKeys(fechaFormat.format(calendar.getTime()));

        //this.driver.findElement(fechaInicioLocator).click();
        this.driver.findElement(buscarBtnLocator).click();

        WebElement btnExportar = fwait.until((WebDriver driver2) -> driver2.findElement(exportarBtnLocator));

        if (this.driver.findElement(exportarBtnLocator).isDisplayed()) {

            this.driver.findElement(exportarBtnLocator).click();

            fwait.until(ExpectedConditions.alertIsPresent());

            String alert = driver.switchTo().alert().getText();

            System.out.println(alert);

            this.driver.switchTo().alert().accept();

            this.driver.findElement(reportesLinkLocator).click();

            WebElement btnDescargar = fwait.until((WebDriver driver3) -> driver3.findElement(descargarLinkLocator));

//            btnDescargar.click();
            System.out.println(btnDescargar.getAttribute("href"));

            this.download(btnDescargar.getAttribute("href"), downloadFilePath);

            //new WebDriverWait(driver, Duration.ofSeconds(10))
            //        .ignoring(NoAlertPresentException.class)
            //       .until(ExpectedConditions.alertIsPresent());
            //WebElement alert = fwait.until((WebDriver driver1) -> driver1.findElement(exportarBtnLocator));
            //this.driver.switchTo().alert().accept();
        }

    }

    public void download(String fileURL, String saveDir) throws IOException {

        URL url = new URL(fileURL);

        //HttpURLConnection httpConn = (HttpURLConnection) (new URL(link).openConnection());
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileName;

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            System.out.println("File downloaded");

            //Path source = Paths.get("/home/mkyong/zip/test.zip");
            //Path target = Paths.get("/home/mkyong/zip/");
            this.unzipFolderZip4j(Paths.get(saveFilePath), Paths.get(downloadFilePath));

            int pos1 = saveFilePath.indexOf("_");
            int pos2 = saveFilePath.indexOf(".");

            //File fileFinal = new File(saveDir + File.separator + "reporte" + saveFilePath.substring(pos1, pos2) + ".xls");
            this.save(new File(saveDir + File.separator + "reporte" + saveFilePath.substring(pos1, pos2) + ".xls"));

        } else {

            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        }

        httpConn.disconnect();
    }

    public void unzipFolderZip4j(Path source, Path target) throws IOException {

        new ZipFile(source.toFile()).extractAll(target.toString());

    }

    public void close() {
        this.driver.close();
    }

    public void run() throws IOException, URISyntaxException, InterruptedException {

        this.setup();

        this.registerUser();

        this.close();

        //System.exit(0);
    }

    public boolean save(File fileFinal) {

        //File fileFinal = new File("/home/hsalinas/Downloads/reporte_98759731.xls");
        if (fileFinal.exists() && !fileFinal.isDirectory()) {

            return aceptaReporteController.create(fileFinal);
        }

        return false;
    }

//    public void run() {
//
//        /**
//         * *
//         */
//        /*
//        WebElement fechaInicio = driver.findElement(By.name("FSTART"));
//
//        fechaInicio.clear();
//        fechaInicio.sendKeys("01072023");
//        fechaInicio.click();
//
//        By buscar = By.xpath("//input[@name='buscar2']");
//        Thread.sleep(2000);
//        driver.findElement(buscar).click();
//
//        Thread.sleep(5000);
//        By exportar = By.xpath("//input[@name='Exportar']");
//        driver.findElement(exportar).click();
//
//        Thread.sleep(2000);
//        driver.switchTo().alert().accept();
//
//        Thread.sleep(2000);
//        By reportes = By.linkText("Reportes");
//
//        driver.findElement(reportes).click();
//
//        //a[@id="1Descargargrilla_reportesNEW"]        
//         */
//    }
}
