/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "orden_compras")
@NamedQueries({
    @NamedQuery(name = "OrdenCompras.findAll", query = "SELECT o FROM OrdenCompras o")})
public class OrdenCompras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Lob
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @Column(name = "order_number")
    private String orderNumber;
    @Basic(optional = false)
    @Column(name = "currency")
    private String currency;
    @Basic(optional = false)
    @Column(name = "tipo")
    private int tipo;
    @Basic(optional = false)
    @Column(name = "total")
    private double total;
    @Basic(optional = false)
    @Column(name = "sigfe")
    private int sigfe;
    @Basic(optional = false)
    @Column(name = "automatic")
    private int automatic;
    @Basic(optional = false)
    @Column(name = "send")
    private int send;
    @JoinColumn(name = "solicitude_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Solicitudes solicitudes;
    @JoinColumn(name = "sga_unidade_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaUnidades sgaUnidades;
    @JoinColumn(name = "sga_departamento_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaDepartamentos sgaDepartamentos;
    @JoinColumn(name = "sga_divisione_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaDivisiones sgaDivisiones;
    @JoinColumn(name = "proveedore_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Proveedores proveedores;
    @OneToMany(mappedBy = "ordenCompras", fetch = FetchType.LAZY)
    private Set<AceptaFactura> aceptaFacturaSet;

    public OrdenCompras() {
    }

    public OrdenCompras(Integer id) {
        this.id = id;
    }

    public OrdenCompras(Integer id, Date created, Date modified, String name, String description, Date date, String orderNumber, String currency, int tipo, double total, int sigfe, int automatic, int send) {
        this.id = id;
        this.created = created;
        this.modified = modified;
        this.name = name;
        this.description = description;
        this.date = date;
        this.orderNumber = orderNumber;
        this.currency = currency;
        this.tipo = tipo;
        this.total = total;
        this.sigfe = sigfe;
        this.automatic = automatic;
        this.send = send;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getSigfe() {
        return sigfe;
    }

    public void setSigfe(int sigfe) {
        this.sigfe = sigfe;
    }

    public int getAutomatic() {
        return automatic;
    }

    public void setAutomatic(int automatic) {
        this.automatic = automatic;
    }

    public int getSend() {
        return send;
    }

    public void setSend(int send) {
        this.send = send;
    }

    public Solicitudes getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(Solicitudes solicitudes) {
        this.solicitudes = solicitudes;
    }

    public SgaUnidades getSgaUnidades() {
        return sgaUnidades;
    }

    public void setSgaUnidades(SgaUnidades sgaUnidades) {
        this.sgaUnidades = sgaUnidades;
    }

    public SgaDepartamentos getSgaDepartamentos() {
        return sgaDepartamentos;
    }

    public void setSgaDepartamentos(SgaDepartamentos sgaDepartamentos) {
        this.sgaDepartamentos = sgaDepartamentos;
    }

    public SgaDivisiones getSgaDivisiones() {
        return sgaDivisiones;
    }

    public void setSgaDivisiones(SgaDivisiones sgaDivisiones) {
        this.sgaDivisiones = sgaDivisiones;
    }

    public Proveedores getProveedores() {
        return proveedores;
    }

    public void setProveedores(Proveedores proveedores) {
        this.proveedores = proveedores;
    }

    public Set<AceptaFactura> getAceptaFacturaSet() {
        return aceptaFacturaSet;
    }

    public void setAceptaFacturaSet(Set<AceptaFactura> aceptaFacturaSet) {
        this.aceptaFacturaSet = aceptaFacturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenCompras)) {
            return false;
        }
        OrdenCompras other = (OrdenCompras) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.OrdenCompras[ id=" + id + " ]";
    }
    
}
