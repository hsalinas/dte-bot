/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "acepta_factura")
@NamedQueries({
    @NamedQuery(name = "AceptaFactura.findAll", query = "SELECT a FROM AceptaFactura a")})
public class AceptaFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "old_id")
    private Integer oldId;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Column(name = "sent_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentDate;
    @Basic(optional = false)
    @Lob
    @Column(name = "content")
    private String content;
    @Basic(optional = false)
    @Column(name = "numero_documento")
    private double numeroDocumento;
    @Basic(optional = false)
    @Column(name = "numero_oc")
    private String numeroOc;
    @Column(name = "fecha_documento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDocumento;
    @Basic(optional = false)
    @Column(name = "numero_dias")
    private int numeroDias;
    @Basic(optional = false)
    @Column(name = "monto_neto")
    private int montoNeto;
    @Basic(optional = false)
    @Column(name = "monto_exento")
    private int montoExento;
    @Basic(optional = false)
    @Column(name = "monto_iva")
    private int montoIva;
    @Basic(optional = false)
    @Column(name = "monto_total")
    private int montoTotal;
    @Basic(optional = false)
    @Column(name = "impuestos")
    private int impuestos;
    @Column(name = "uri")
    private String uri;
    @Column(name = "fecha_recepcion_sii")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcionSii;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Column(name = "fecha_aceptacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAceptacion;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "resetear")
    private Integer resetear;
    @Basic(optional = false)
    @Column(name = "dte_check")
    private boolean dteCheck;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @JoinColumn(name = "acepta_folder_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaFolder aceptaFolder;
    @JoinColumn(name = "acepta_tipo_dte_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaTipoDte aceptaTipoDte;
    @JoinColumn(name = "orden_compra_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OrdenCompras ordenCompras;
    @JoinColumn(name = "acepta_tipo_carga_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaTipoCarga aceptaTipoCarga;
    @JoinColumn(name = "proveedor_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Proveedores proveedores;
    @JoinColumn(name = "acepta_estado_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaEstado aceptaEstado;
    @JoinColumn(name = "acepta_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaGroup aceptaGroup;
    @JoinColumn(name = "acepta_flow_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaFlow aceptaFlow;
    @JoinColumn(name = "programa_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Programas programas;
    @JoinColumn(name = "dte_flow_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DtesFlows dtesFlows;
    @JoinColumn(name = "solicitud_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Solicitudes solicitudes;

    public AceptaFactura() {
    }

    public AceptaFactura(Integer id) {
        this.id = id;
    }

    public AceptaFactura(Integer id, Date created, Date modified, String content, double numeroDocumento, String numeroOc, int numeroDias, int montoNeto, int montoExento, int montoIva, int montoTotal, int impuestos, boolean dteCheck, boolean enabled) {
        this.id = id;
        this.created = created;
        this.modified = modified;
        this.content = content;
        this.numeroDocumento = numeroDocumento;
        this.numeroOc = numeroOc;
        this.numeroDias = numeroDias;
        this.montoNeto = montoNeto;
        this.montoExento = montoExento;
        this.montoIva = montoIva;
        this.montoTotal = montoTotal;
        this.impuestos = impuestos;
        this.dteCheck = dteCheck;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOldId() {
        return oldId;
    }

    public void setOldId(Integer oldId) {
        this.oldId = oldId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(double numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNumeroOc() {
        return numeroOc;
    }

    public void setNumeroOc(String numeroOc) {
        this.numeroOc = numeroOc;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public int getNumeroDias() {
        return numeroDias;
    }

    public void setNumeroDias(int numeroDias) {
        this.numeroDias = numeroDias;
    }

    public int getMontoNeto() {
        return montoNeto;
    }

    public void setMontoNeto(int montoNeto) {
        this.montoNeto = montoNeto;
    }

    public int getMontoExento() {
        return montoExento;
    }

    public void setMontoExento(int montoExento) {
        this.montoExento = montoExento;
    }

    public int getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(int montoIva) {
        this.montoIva = montoIva;
    }

    public int getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(int montoTotal) {
        this.montoTotal = montoTotal;
    }

    public int getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(int impuestos) {
        this.impuestos = impuestos;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Date getFechaRecepcionSii() {
        return fechaRecepcionSii;
    }

    public void setFechaRecepcionSii(Date fechaRecepcionSii) {
        this.fechaRecepcionSii = fechaRecepcionSii;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaAceptacion() {
        return fechaAceptacion;
    }

    public void setFechaAceptacion(Date fechaAceptacion) {
        this.fechaAceptacion = fechaAceptacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getResetear() {
        return resetear;
    }

    public void setResetear(Integer resetear) {
        this.resetear = resetear;
    }

    public boolean getDteCheck() {
        return dteCheck;
    }

    public void setDteCheck(boolean dteCheck) {
        this.dteCheck = dteCheck;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public AceptaFolder getAceptaFolder() {
        return aceptaFolder;
    }

    public void setAceptaFolder(AceptaFolder aceptaFolder) {
        this.aceptaFolder = aceptaFolder;
    }

    public AceptaTipoDte getAceptaTipoDte() {
        return aceptaTipoDte;
    }

    public void setAceptaTipoDte(AceptaTipoDte aceptaTipoDte) {
        this.aceptaTipoDte = aceptaTipoDte;
    }

    public OrdenCompras getOrdenCompras() {
        return ordenCompras;
    }

    public void setOrdenCompras(OrdenCompras ordenCompras) {
        this.ordenCompras = ordenCompras;
    }

    public AceptaTipoCarga getAceptaTipoCarga() {
        return aceptaTipoCarga;
    }

    public void setAceptaTipoCarga(AceptaTipoCarga aceptaTipoCarga) {
        this.aceptaTipoCarga = aceptaTipoCarga;
    }

    public Proveedores getProveedores() {
        return proveedores;
    }

    public void setProveedores(Proveedores proveedores) {
        this.proveedores = proveedores;
    }

    public AceptaEstado getAceptaEstado() {
        return aceptaEstado;
    }

    public void setAceptaEstado(AceptaEstado aceptaEstado) {
        this.aceptaEstado = aceptaEstado;
    }

    public AceptaGroup getAceptaGroup() {
        return aceptaGroup;
    }

    public void setAceptaGroup(AceptaGroup aceptaGroup) {
        this.aceptaGroup = aceptaGroup;
    }

    public AceptaFlow getAceptaFlow() {
        return aceptaFlow;
    }

    public void setAceptaFlow(AceptaFlow aceptaFlow) {
        this.aceptaFlow = aceptaFlow;
    }

    public Programas getProgramas() {
        return programas;
    }

    public void setProgramas(Programas programas) {
        this.programas = programas;
    }

    public DtesFlows getDtesFlows() {
        return dtesFlows;
    }

    public void setDtesFlows(DtesFlows dtesFlows) {
        this.dtesFlows = dtesFlows;
    }

    public Solicitudes getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(Solicitudes solicitudes) {
        this.solicitudes = solicitudes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AceptaFactura)) {
            return false;
        }
        AceptaFactura other = (AceptaFactura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.AceptaFactura[ id=" + id + " ]";
    }
    
}
