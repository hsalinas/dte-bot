/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "sga_departamentos")
@NamedQueries({
    @NamedQuery(name = "SgaDepartamentos.findAll", query = "SELECT s FROM SgaDepartamentos s")})
public class SgaDepartamentos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @Basic(optional = false)
    @Column(name = "existe")
    private boolean existe;
    @OneToMany(mappedBy = "sgaDepartamentos", fetch = FetchType.LAZY)
    private Set<SgaUsers> sgaUsersSet;
    @OneToMany(mappedBy = "sgaDepartamentos", fetch = FetchType.LAZY)
    private Set<SgaUnidades> sgaUnidadesSet;
    @OneToMany(mappedBy = "sgaDepartamentos", fetch = FetchType.LAZY)
    private Set<Solicitudes> solicitudesSet;
    @OneToMany(mappedBy = "sgaDepartamentos", fetch = FetchType.LAZY)
    private Set<OrdenCompras> ordenComprasSet;
    @JoinColumn(name = "sga_divisione_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaDivisiones sgaDivisiones;

    public SgaDepartamentos() {
    }

    public SgaDepartamentos(Integer id) {
        this.id = id;
    }

    public SgaDepartamentos(Integer id, String name, Date created, Date modified, String codigo, boolean enabled, boolean existe) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.codigo = codigo;
        this.enabled = enabled;
        this.existe = existe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean getExiste() {
        return existe;
    }

    public void setExiste(boolean existe) {
        this.existe = existe;
    }

    public Set<SgaUsers> getSgaUsersSet() {
        return sgaUsersSet;
    }

    public void setSgaUsersSet(Set<SgaUsers> sgaUsersSet) {
        this.sgaUsersSet = sgaUsersSet;
    }

    public Set<SgaUnidades> getSgaUnidadesSet() {
        return sgaUnidadesSet;
    }

    public void setSgaUnidadesSet(Set<SgaUnidades> sgaUnidadesSet) {
        this.sgaUnidadesSet = sgaUnidadesSet;
    }

    public Set<Solicitudes> getSolicitudesSet() {
        return solicitudesSet;
    }

    public void setSolicitudesSet(Set<Solicitudes> solicitudesSet) {
        this.solicitudesSet = solicitudesSet;
    }

    public Set<OrdenCompras> getOrdenComprasSet() {
        return ordenComprasSet;
    }

    public void setOrdenComprasSet(Set<OrdenCompras> ordenComprasSet) {
        this.ordenComprasSet = ordenComprasSet;
    }

    public SgaDivisiones getSgaDivisiones() {
        return sgaDivisiones;
    }

    public void setSgaDivisiones(SgaDivisiones sgaDivisiones) {
        this.sgaDivisiones = sgaDivisiones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SgaDepartamentos)) {
            return false;
        }
        SgaDepartamentos other = (SgaDepartamentos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.SgaDepartamentos[ id=" + id + " ]";
    }
    
}
