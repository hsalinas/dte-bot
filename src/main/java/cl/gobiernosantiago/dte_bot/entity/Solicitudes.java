/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "solicitudes")
@NamedQueries({
    @NamedQuery(name = "Solicitudes.findAll", query = "SELECT s FROM Solicitudes s")})
public class Solicitudes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Column(name = "sent_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentDate;
    @Basic(optional = false)
    @Column(name = "centro_costo")
    private String centroCosto;
    @Basic(optional = false)
    @Lob
    @Column(name = "justificacion")
    private String justificacion;
    @Column(name = "derivado")
    private Integer derivado;
    @Basic(optional = false)
    @Column(name = "derivado_name")
    private String derivadoName;
    @Basic(optional = false)
    @Column(name = "method")
    private String method;
    @Basic(optional = false)
    @Lob
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @Column(name = "ubicacion")
    private int ubicacion;
    @Basic(optional = false)
    @Lob
    @Column(name = "cuadro")
    private String cuadro;
    @Basic(optional = false)
    @Column(name = "ciclo")
    private int ciclo;
    @Basic(optional = false)
    @Column(name = "clonar")
    private int clonar;
    @Column(name = "fecha_resolucion")
    @Temporal(TemporalType.DATE)
    private Date fechaResolucion;
    @Basic(optional = false)
    @Column(name = "resolucion")
    private int resolucion;
    @Basic(optional = false)
    @Lob
    @Column(name = "organizacion")
    private String organizacion;
    @Basic(optional = false)
    @Lob
    @Column(name = "proyecto")
    private String proyecto;
    @Basic(optional = false)
    @Column(name = "pagado")
    private boolean pagado;
    @Lob
    @Column(name = "content")
    private String content;
    @JoinColumn(name = "sga_divisione_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaDivisiones sgaDivisiones;
    @JoinColumn(name = "programa_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Programas programas;
    @JoinColumn(name = "tipo_compra_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoCompras tipoCompras;
    @JoinColumn(name = "sga_unidade_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaUnidades sgaUnidades;
    @JoinColumn(name = "sga_departamento_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaDepartamentos sgaDepartamentos;
    @JoinColumn(name = "sga_flow_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaFlows sgaFlows;
    @JoinColumn(name = "analista_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Analistas analistas;
    @JoinColumn(name = "sga_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaGroups sgaGroups;
    @JoinColumn(name = "sga_secuencia_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaSecuencias sgaSecuencias;
    @JoinColumn(name = "sga_user_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaUsers sgaUsers;
    @JoinColumn(name = "sga_folder_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaFolders sgaFolders;
    @JoinColumn(name = "sub_programa_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SubProgramas subProgramas;
    @JoinColumn(name = "sga_estado_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaEstados sgaEstados;
    @JoinColumn(name = "sga_periodo_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaPeriodos sgaPeriodos;
    @OneToMany(mappedBy = "solicitudes", fetch = FetchType.LAZY)
    private Set<OrdenCompras> ordenComprasSet;
    @OneToMany(mappedBy = "solicitudes", fetch = FetchType.LAZY)
    private Set<SgaSecuencias> sgaSecuenciasSet;
    @OneToMany(mappedBy = "solicitudes", fetch = FetchType.LAZY)
    private Set<AceptaFactura> aceptaFacturaSet;

    public Solicitudes() {
    }

    public Solicitudes(Integer id) {
        this.id = id;
    }

    public Solicitudes(Integer id, String name, Date created, Date modified, String centroCosto, String justificacion, String derivadoName, String method, String observacion, int ubicacion, String cuadro, int ciclo, int clonar, int resolucion, String organizacion, String proyecto, boolean pagado) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.centroCosto = centroCosto;
        this.justificacion = justificacion;
        this.derivadoName = derivadoName;
        this.method = method;
        this.observacion = observacion;
        this.ubicacion = ubicacion;
        this.cuadro = cuadro;
        this.ciclo = ciclo;
        this.clonar = clonar;
        this.resolucion = resolucion;
        this.organizacion = organizacion;
        this.proyecto = proyecto;
        this.pagado = pagado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getCentroCosto() {
        return centroCosto;
    }

    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public Integer getDerivado() {
        return derivado;
    }

    public void setDerivado(Integer derivado) {
        this.derivado = derivado;
    }

    public String getDerivadoName() {
        return derivadoName;
    }

    public void setDerivadoName(String derivadoName) {
        this.derivadoName = derivadoName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(int ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getCuadro() {
        return cuadro;
    }

    public void setCuadro(String cuadro) {
        this.cuadro = cuadro;
    }

    public int getCiclo() {
        return ciclo;
    }

    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    public int getClonar() {
        return clonar;
    }

    public void setClonar(int clonar) {
        this.clonar = clonar;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public int getResolucion() {
        return resolucion;
    }

    public void setResolucion(int resolucion) {
        this.resolucion = resolucion;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public boolean getPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public SgaDivisiones getSgaDivisiones() {
        return sgaDivisiones;
    }

    public void setSgaDivisiones(SgaDivisiones sgaDivisiones) {
        this.sgaDivisiones = sgaDivisiones;
    }

    public Programas getProgramas() {
        return programas;
    }

    public void setProgramas(Programas programas) {
        this.programas = programas;
    }

    public TipoCompras getTipoCompras() {
        return tipoCompras;
    }

    public void setTipoCompras(TipoCompras tipoCompras) {
        this.tipoCompras = tipoCompras;
    }

    public SgaUnidades getSgaUnidades() {
        return sgaUnidades;
    }

    public void setSgaUnidades(SgaUnidades sgaUnidades) {
        this.sgaUnidades = sgaUnidades;
    }

    public SgaDepartamentos getSgaDepartamentos() {
        return sgaDepartamentos;
    }

    public void setSgaDepartamentos(SgaDepartamentos sgaDepartamentos) {
        this.sgaDepartamentos = sgaDepartamentos;
    }

    public SgaFlows getSgaFlows() {
        return sgaFlows;
    }

    public void setSgaFlows(SgaFlows sgaFlows) {
        this.sgaFlows = sgaFlows;
    }

    public Analistas getAnalistas() {
        return analistas;
    }

    public void setAnalistas(Analistas analistas) {
        this.analistas = analistas;
    }

    public SgaGroups getSgaGroups() {
        return sgaGroups;
    }

    public void setSgaGroups(SgaGroups sgaGroups) {
        this.sgaGroups = sgaGroups;
    }

    public SgaSecuencias getSgaSecuencias() {
        return sgaSecuencias;
    }

    public void setSgaSecuencias(SgaSecuencias sgaSecuencias) {
        this.sgaSecuencias = sgaSecuencias;
    }

    public SgaUsers getSgaUsers() {
        return sgaUsers;
    }

    public void setSgaUsers(SgaUsers sgaUsers) {
        this.sgaUsers = sgaUsers;
    }

    public SgaFolders getSgaFolders() {
        return sgaFolders;
    }

    public void setSgaFolders(SgaFolders sgaFolders) {
        this.sgaFolders = sgaFolders;
    }

    public SubProgramas getSubProgramas() {
        return subProgramas;
    }

    public void setSubProgramas(SubProgramas subProgramas) {
        this.subProgramas = subProgramas;
    }

    public SgaEstados getSgaEstados() {
        return sgaEstados;
    }

    public void setSgaEstados(SgaEstados sgaEstados) {
        this.sgaEstados = sgaEstados;
    }

    public SgaPeriodos getSgaPeriodos() {
        return sgaPeriodos;
    }

    public void setSgaPeriodos(SgaPeriodos sgaPeriodos) {
        this.sgaPeriodos = sgaPeriodos;
    }

    public Set<OrdenCompras> getOrdenComprasSet() {
        return ordenComprasSet;
    }

    public void setOrdenComprasSet(Set<OrdenCompras> ordenComprasSet) {
        this.ordenComprasSet = ordenComprasSet;
    }

    public Set<SgaSecuencias> getSgaSecuenciasSet() {
        return sgaSecuenciasSet;
    }

    public void setSgaSecuenciasSet(Set<SgaSecuencias> sgaSecuenciasSet) {
        this.sgaSecuenciasSet = sgaSecuenciasSet;
    }

    public Set<AceptaFactura> getAceptaFacturaSet() {
        return aceptaFacturaSet;
    }

    public void setAceptaFacturaSet(Set<AceptaFactura> aceptaFacturaSet) {
        this.aceptaFacturaSet = aceptaFacturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Solicitudes)) {
            return false;
        }
        Solicitudes other = (Solicitudes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.Solicitudes[ id=" + id + " ]";
    }
    
}
