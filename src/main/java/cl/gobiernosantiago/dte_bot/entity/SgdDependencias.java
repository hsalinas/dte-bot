/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "sgd_dependencias")
@NamedQueries({
    @NamedQuery(name = "SgdDependencias.findAll", query = "SELECT s FROM SgdDependencias s")})
public class SgdDependencias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @OneToMany(mappedBy = "sgdDependencias", fetch = FetchType.LAZY)
    private Set<SgaGroups> sgaGroupsSet;
    @OneToMany(mappedBy = "sgdDependencias", fetch = FetchType.LAZY)
    private Set<SgaUsers> sgaUsersSet;
    @OneToMany(mappedBy = "sgdDependencias", fetch = FetchType.LAZY)
    private Set<SgdGroups> sgdGroupsSet;

    public SgdDependencias() {
    }

    public SgdDependencias(Integer id) {
        this.id = id;
    }

    public SgdDependencias(Integer id, String name, Date created, Date modified, boolean enabled) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<SgaGroups> getSgaGroupsSet() {
        return sgaGroupsSet;
    }

    public void setSgaGroupsSet(Set<SgaGroups> sgaGroupsSet) {
        this.sgaGroupsSet = sgaGroupsSet;
    }

    public Set<SgaUsers> getSgaUsersSet() {
        return sgaUsersSet;
    }

    public void setSgaUsersSet(Set<SgaUsers> sgaUsersSet) {
        this.sgaUsersSet = sgaUsersSet;
    }

    public Set<SgdGroups> getSgdGroupsSet() {
        return sgdGroupsSet;
    }

    public void setSgdGroupsSet(Set<SgdGroups> sgdGroupsSet) {
        this.sgdGroupsSet = sgdGroupsSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SgdDependencias)) {
            return false;
        }
        SgdDependencias other = (SgdDependencias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.SgdDependencias[ id=" + id + " ]";
    }
    
}
