/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "dtes_groups")
@NamedQueries({
    @NamedQuery(name = "DtesGroups.findAll", query = "SELECT d FROM DtesGroups d")})
public class DtesGroups implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "crear")
    private Boolean crear;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @OneToMany(mappedBy = "dtesGroups", fetch = FetchType.LAZY)
    private Set<SgaUsers> sgaUsersSet;
    @JoinColumn(name = "tipo_user_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoUsers tipoUsers;
    @JoinColumn(name = "tipo_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoGroups tipoGroups;
    @OneToMany(mappedBy = "dtesGroups", fetch = FetchType.LAZY)
    private Set<DtesFlows> dtesFlowsSet;

    public DtesGroups() {
    }

    public DtesGroups(Integer id) {
        this.id = id;
    }

    public DtesGroups(Integer id, String name, Date created, Date modified, String codigo, boolean enabled) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.codigo = codigo;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Boolean getCrear() {
        return crear;
    }

    public void setCrear(Boolean crear) {
        this.crear = crear;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<SgaUsers> getSgaUsersSet() {
        return sgaUsersSet;
    }

    public void setSgaUsersSet(Set<SgaUsers> sgaUsersSet) {
        this.sgaUsersSet = sgaUsersSet;
    }

    public TipoUsers getTipoUsers() {
        return tipoUsers;
    }

    public void setTipoUsers(TipoUsers tipoUsers) {
        this.tipoUsers = tipoUsers;
    }

    public TipoGroups getTipoGroups() {
        return tipoGroups;
    }

    public void setTipoGroups(TipoGroups tipoGroups) {
        this.tipoGroups = tipoGroups;
    }

    public Set<DtesFlows> getDtesFlowsSet() {
        return dtesFlowsSet;
    }

    public void setDtesFlowsSet(Set<DtesFlows> dtesFlowsSet) {
        this.dtesFlowsSet = dtesFlowsSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtesGroups)) {
            return false;
        }
        DtesGroups other = (DtesGroups) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.DtesGroups[ id=" + id + " ]";
    }
    
}
