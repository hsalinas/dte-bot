/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "programas")
@NamedQueries({
    @NamedQuery(name = "Programas.findAll", query = "SELECT p FROM Programas p")})
public class Programas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @OneToMany(mappedBy = "programas", fetch = FetchType.LAZY)
    private Set<Solicitudes> solicitudesSet;
    @OneToMany(mappedBy = "programas", fetch = FetchType.LAZY)
    private Set<AceptaGroup> aceptaGroupSet;
    @OneToMany(mappedBy = "programas", fetch = FetchType.LAZY)
    private Set<DtesFlows> dtesFlowsSet;
    @OneToMany(mappedBy = "programas", fetch = FetchType.LAZY)
    private Set<SgaFlows> sgaFlowsSet;
    @OneToMany(mappedBy = "programas", fetch = FetchType.LAZY)
    private Set<SubProgramas> subProgramasSet;
    @OneToMany(mappedBy = "programas", fetch = FetchType.LAZY)
    private Set<AceptaFactura> aceptaFacturaSet;

    public Programas() {
    }

    public Programas(Integer id) {
        this.id = id;
    }

    public Programas(Integer id, String name, String codigo) {
        this.id = id;
        this.name = name;
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Set<Solicitudes> getSolicitudesSet() {
        return solicitudesSet;
    }

    public void setSolicitudesSet(Set<Solicitudes> solicitudesSet) {
        this.solicitudesSet = solicitudesSet;
    }

    public Set<AceptaGroup> getAceptaGroupSet() {
        return aceptaGroupSet;
    }

    public void setAceptaGroupSet(Set<AceptaGroup> aceptaGroupSet) {
        this.aceptaGroupSet = aceptaGroupSet;
    }

    public Set<DtesFlows> getDtesFlowsSet() {
        return dtesFlowsSet;
    }

    public void setDtesFlowsSet(Set<DtesFlows> dtesFlowsSet) {
        this.dtesFlowsSet = dtesFlowsSet;
    }

    public Set<SgaFlows> getSgaFlowsSet() {
        return sgaFlowsSet;
    }

    public void setSgaFlowsSet(Set<SgaFlows> sgaFlowsSet) {
        this.sgaFlowsSet = sgaFlowsSet;
    }

    public Set<SubProgramas> getSubProgramasSet() {
        return subProgramasSet;
    }

    public void setSubProgramasSet(Set<SubProgramas> subProgramasSet) {
        this.subProgramasSet = subProgramasSet;
    }

    public Set<AceptaFactura> getAceptaFacturaSet() {
        return aceptaFacturaSet;
    }

    public void setAceptaFacturaSet(Set<AceptaFactura> aceptaFacturaSet) {
        this.aceptaFacturaSet = aceptaFacturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Programas)) {
            return false;
        }
        Programas other = (Programas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.Programas[ id=" + id + " ]";
    }
    
}
