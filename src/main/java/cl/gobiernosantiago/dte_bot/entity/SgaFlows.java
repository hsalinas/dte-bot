/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "sga_flows")
@NamedQueries({
    @NamedQuery(name = "SgaFlows.findAll", query = "SELECT s FROM SgaFlows s")})
public class SgaFlows implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "old_group_id")
    private int oldGroupId;
    @Basic(optional = false)
    @Column(name = "full_name")
    private String fullName;
    @Basic(optional = false)
    @Column(name = "orden")
    private int orden;
    @Basic(optional = false)
    @Column(name = "method")
    private String method;
    @Basic(optional = false)
    @Column(name = "action")
    private String action;
    @Column(name = "old_id")
    private Integer oldId;
    @OneToMany(mappedBy = "sgaFlows", fetch = FetchType.LAZY)
    private Set<Solicitudes> solicitudesSet;
    @JoinColumn(name = "sga_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaGroups sgaGroups;
    @JoinColumn(name = "programa_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Programas programas;

    public SgaFlows() {
    }

    public SgaFlows(Integer id) {
        this.id = id;
    }

    public SgaFlows(Integer id, String name, Date created, Date modified, int oldGroupId, String fullName, int orden, String method, String action) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.oldGroupId = oldGroupId;
        this.fullName = fullName;
        this.orden = orden;
        this.method = method;
        this.action = action;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public int getOldGroupId() {
        return oldGroupId;
    }

    public void setOldGroupId(int oldGroupId) {
        this.oldGroupId = oldGroupId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getOldId() {
        return oldId;
    }

    public void setOldId(Integer oldId) {
        this.oldId = oldId;
    }

    public Set<Solicitudes> getSolicitudesSet() {
        return solicitudesSet;
    }

    public void setSolicitudesSet(Set<Solicitudes> solicitudesSet) {
        this.solicitudesSet = solicitudesSet;
    }

    public SgaGroups getSgaGroups() {
        return sgaGroups;
    }

    public void setSgaGroups(SgaGroups sgaGroups) {
        this.sgaGroups = sgaGroups;
    }

    public Programas getProgramas() {
        return programas;
    }

    public void setProgramas(Programas programas) {
        this.programas = programas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SgaFlows)) {
            return false;
        }
        SgaFlows other = (SgaFlows) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.SgaFlows[ id=" + id + " ]";
    }
    
}
