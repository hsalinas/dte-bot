/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "sga_periodos")
@NamedQueries({
    @NamedQuery(name = "SgaPeriodos.findAll", query = "SELECT s FROM SgaPeriodos s")})
public class SgaPeriodos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "anio")
    private String anio;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "ley")
    private int ley;
    @Basic(optional = false)
    @Column(name = "impuesto")
    private double impuesto;
    @Basic(optional = false)
    @Column(name = "postulacion_enabled")
    private boolean postulacionEnabled;
    @OneToMany(mappedBy = "sgaPeriodos", fetch = FetchType.LAZY)
    private Set<Solicitudes> solicitudesSet;

    public SgaPeriodos() {
    }

    public SgaPeriodos(Integer id) {
        this.id = id;
    }

    public SgaPeriodos(Integer id, String anio, Date created, Date modified, int ley, double impuesto, boolean postulacionEnabled) {
        this.id = id;
        this.anio = anio;
        this.created = created;
        this.modified = modified;
        this.ley = ley;
        this.impuesto = impuesto;
        this.postulacionEnabled = postulacionEnabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public int getLey() {
        return ley;
    }

    public void setLey(int ley) {
        this.ley = ley;
    }

    public double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }

    public boolean getPostulacionEnabled() {
        return postulacionEnabled;
    }

    public void setPostulacionEnabled(boolean postulacionEnabled) {
        this.postulacionEnabled = postulacionEnabled;
    }

    public Set<Solicitudes> getSolicitudesSet() {
        return solicitudesSet;
    }

    public void setSolicitudesSet(Set<Solicitudes> solicitudesSet) {
        this.solicitudesSet = solicitudesSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SgaPeriodos)) {
            return false;
        }
        SgaPeriodos other = (SgaPeriodos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.SgaPeriodos[ id=" + id + " ]";
    }
    
}
