/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "acepta_group")
@NamedQueries({
    @NamedQuery(name = "AceptaGroup.findAll", query = "SELECT a FROM AceptaGroup a")})
public class AceptaGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "alias")
    private String alias;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @OneToMany(mappedBy = "aceptaGroup", fetch = FetchType.LAZY)
    private Set<SgaUsers> sgaUsersSet;
    @OneToMany(mappedBy = "aceptaGroup", fetch = FetchType.LAZY)
    private Set<AceptaFlow> aceptaFlowSet;
    @OneToMany(mappedBy = "aceptaGroup", fetch = FetchType.LAZY)
    private Set<AceptaGroup> aceptaGroupSet;
    @JoinColumn(name = "node_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaGroup aceptaGroup;
    @JoinColumn(name = "sga_user_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaUsers sgaUsers;
    @JoinColumn(name = "tipo_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoGroups tipoGroups;
    @JoinColumn(name = "programa_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Programas programas;
    @OneToMany(mappedBy = "aceptaGroup", fetch = FetchType.LAZY)
    private Set<AceptaFactura> aceptaFacturaSet;

    public AceptaGroup() {
    }

    public AceptaGroup(Integer id) {
        this.id = id;
    }

    public AceptaGroup(Integer id, String name, Date created, Date modified, String codigo, String alias, boolean enabled) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.codigo = codigo;
        this.alias = alias;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<SgaUsers> getSgaUsersSet() {
        return sgaUsersSet;
    }

    public void setSgaUsersSet(Set<SgaUsers> sgaUsersSet) {
        this.sgaUsersSet = sgaUsersSet;
    }

    public Set<AceptaFlow> getAceptaFlowSet() {
        return aceptaFlowSet;
    }

    public void setAceptaFlowSet(Set<AceptaFlow> aceptaFlowSet) {
        this.aceptaFlowSet = aceptaFlowSet;
    }

    public Set<AceptaGroup> getAceptaGroupSet() {
        return aceptaGroupSet;
    }

    public void setAceptaGroupSet(Set<AceptaGroup> aceptaGroupSet) {
        this.aceptaGroupSet = aceptaGroupSet;
    }

    public AceptaGroup getAceptaGroup() {
        return aceptaGroup;
    }

    public void setAceptaGroup(AceptaGroup aceptaGroup) {
        this.aceptaGroup = aceptaGroup;
    }

    public SgaUsers getSgaUsers() {
        return sgaUsers;
    }

    public void setSgaUsers(SgaUsers sgaUsers) {
        this.sgaUsers = sgaUsers;
    }

    public TipoGroups getTipoGroups() {
        return tipoGroups;
    }

    public void setTipoGroups(TipoGroups tipoGroups) {
        this.tipoGroups = tipoGroups;
    }

    public Programas getProgramas() {
        return programas;
    }

    public void setProgramas(Programas programas) {
        this.programas = programas;
    }

    public Set<AceptaFactura> getAceptaFacturaSet() {
        return aceptaFacturaSet;
    }

    public void setAceptaFacturaSet(Set<AceptaFactura> aceptaFacturaSet) {
        this.aceptaFacturaSet = aceptaFacturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AceptaGroup)) {
            return false;
        }
        AceptaGroup other = (AceptaGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.AceptaGroup[ id=" + id + " ]";
    }
    
}
