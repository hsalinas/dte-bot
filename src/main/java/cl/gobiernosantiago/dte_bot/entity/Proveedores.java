/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "proveedores")
@NamedQueries({
    @NamedQuery(name = "Proveedores.findAll", query = "SELECT p FROM Proveedores p")})
public class Proveedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "razon_social")
    private String razonSocial;
    @Basic(optional = false)
    @Column(name = "rut")
    private String rut;
    @Column(name = "nombre_fantasia")
    private String nombreFantasia;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "fax")
    private String fax;
    @Column(name = "website")
    private String website;
    @Column(name = "email")
    private String email;
    @Column(name = "nombre_contacto")
    private String nombreContacto;
    @Column(name = "telefono_contacto")
    private String telefonoContacto;
    @Column(name = "celular_contacto")
    private String celularContacto;
    @Basic(optional = false)
    @Column(name = "cargo_contacto")
    private String cargoContacto;
    @Basic(optional = false)
    @Column(name = "is_active")
    private boolean isActive;
    @Column(name = "cuenta")
    private String cuenta;
    @Basic(optional = false)
    @Column(name = "banco_id_old")
    private int bancoIdOld;
    @Basic(optional = false)
    @Column(name = "ciudade_id")
    private int ciudadeId;
    @Basic(optional = false)
    @Column(name = "comuna_id")
    private int comunaId;
    @Basic(optional = false)
    @Column(name = "categoria")
    private String categoria;
    @Basic(optional = false)
    @Column(name = "pago_retenido")
    private int pagoRetenido;
    @JoinColumn(name = "banco_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Bancos bancos;
    @JoinColumn(name = "formapago_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Formapagos formapagos;
    @JoinColumn(name = "sector_financiero_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SectorFinancieros sectorFinancieros;
    @OneToMany(mappedBy = "proveedores", fetch = FetchType.LAZY)
    private Set<OrdenCompras> ordenComprasSet;
    @OneToMany(mappedBy = "proveedores", fetch = FetchType.LAZY)
    private Set<AceptaFactura> aceptaFacturaSet;

    public Proveedores() {
    }

    public Proveedores(Integer id) {
        this.id = id;
    }

    public Proveedores(Integer id, String razonSocial, String rut, String cargoContacto, boolean isActive, int bancoIdOld, int ciudadeId, int comunaId, String categoria, int pagoRetenido) {
        this.id = id;
        this.razonSocial = razonSocial;
        this.rut = rut;
        this.cargoContacto = cargoContacto;
        this.isActive = isActive;
        this.bancoIdOld = bancoIdOld;
        this.ciudadeId = ciudadeId;
        this.comunaId = comunaId;
        this.categoria = categoria;
        this.pagoRetenido = pagoRetenido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public void setNombreFantasia(String nombreFantasia) {
        this.nombreFantasia = nombreFantasia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getCelularContacto() {
        return celularContacto;
    }

    public void setCelularContacto(String celularContacto) {
        this.celularContacto = celularContacto;
    }

    public String getCargoContacto() {
        return cargoContacto;
    }

    public void setCargoContacto(String cargoContacto) {
        this.cargoContacto = cargoContacto;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public int getBancoIdOld() {
        return bancoIdOld;
    }

    public void setBancoIdOld(int bancoIdOld) {
        this.bancoIdOld = bancoIdOld;
    }

    public int getCiudadeId() {
        return ciudadeId;
    }

    public void setCiudadeId(int ciudadeId) {
        this.ciudadeId = ciudadeId;
    }

    public int getComunaId() {
        return comunaId;
    }

    public void setComunaId(int comunaId) {
        this.comunaId = comunaId;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getPagoRetenido() {
        return pagoRetenido;
    }

    public void setPagoRetenido(int pagoRetenido) {
        this.pagoRetenido = pagoRetenido;
    }

    public Bancos getBancos() {
        return bancos;
    }

    public void setBancos(Bancos bancos) {
        this.bancos = bancos;
    }

    public Formapagos getFormapagos() {
        return formapagos;
    }

    public void setFormapagos(Formapagos formapagos) {
        this.formapagos = formapagos;
    }

    public SectorFinancieros getSectorFinancieros() {
        return sectorFinancieros;
    }

    public void setSectorFinancieros(SectorFinancieros sectorFinancieros) {
        this.sectorFinancieros = sectorFinancieros;
    }

    public Set<OrdenCompras> getOrdenComprasSet() {
        return ordenComprasSet;
    }

    public void setOrdenComprasSet(Set<OrdenCompras> ordenComprasSet) {
        this.ordenComprasSet = ordenComprasSet;
    }

    public Set<AceptaFactura> getAceptaFacturaSet() {
        return aceptaFacturaSet;
    }

    public void setAceptaFacturaSet(Set<AceptaFactura> aceptaFacturaSet) {
        this.aceptaFacturaSet = aceptaFacturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedores)) {
            return false;
        }
        Proveedores other = (Proveedores) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.Proveedores[ id=" + id + " ]";
    }
    
}
