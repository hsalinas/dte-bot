/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "sgd_groups")
@NamedQueries({
    @NamedQuery(name = "SgdGroups.findAll", query = "SELECT s FROM SgdGroups s")})
public class SgdGroups implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "etoken")
    private boolean etoken;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @Basic(optional = false)
    @Column(name = "check_md5sum")
    private boolean checkMd5sum;
    @JoinTable(name = "sgd_compartir", joinColumns = {
        @JoinColumn(name = "sgd_group_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "sga_user_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<SgaUsers> sgaUsersSet;
    @OneToMany(mappedBy = "sgdGroups", fetch = FetchType.LAZY)
    private Set<SgaUsers> sgaUsersSet1;
    @JoinColumn(name = "tipo_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoGroups tipoGroups;
    @JoinColumn(name = "sgd_dependencia_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgdDependencias sgdDependencias;
    @JoinColumn(name = "cargo_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cargos cargos;

    public SgdGroups() {
    }

    public SgdGroups(Integer id) {
        this.id = id;
    }

    public SgdGroups(Integer id, String name, Date created, Date modified, String codigo, boolean etoken, boolean enabled, boolean checkMd5sum) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.codigo = codigo;
        this.etoken = etoken;
        this.enabled = enabled;
        this.checkMd5sum = checkMd5sum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean getEtoken() {
        return etoken;
    }

    public void setEtoken(boolean etoken) {
        this.etoken = etoken;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean getCheckMd5sum() {
        return checkMd5sum;
    }

    public void setCheckMd5sum(boolean checkMd5sum) {
        this.checkMd5sum = checkMd5sum;
    }

    public Set<SgaUsers> getSgaUsersSet() {
        return sgaUsersSet;
    }

    public void setSgaUsersSet(Set<SgaUsers> sgaUsersSet) {
        this.sgaUsersSet = sgaUsersSet;
    }

    public Set<SgaUsers> getSgaUsersSet1() {
        return sgaUsersSet1;
    }

    public void setSgaUsersSet1(Set<SgaUsers> sgaUsersSet1) {
        this.sgaUsersSet1 = sgaUsersSet1;
    }

    public TipoGroups getTipoGroups() {
        return tipoGroups;
    }

    public void setTipoGroups(TipoGroups tipoGroups) {
        this.tipoGroups = tipoGroups;
    }

    public SgdDependencias getSgdDependencias() {
        return sgdDependencias;
    }

    public void setSgdDependencias(SgdDependencias sgdDependencias) {
        this.sgdDependencias = sgdDependencias;
    }

    public Cargos getCargos() {
        return cargos;
    }

    public void setCargos(Cargos cargos) {
        this.cargos = cargos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SgdGroups)) {
            return false;
        }
        SgdGroups other = (SgdGroups) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.SgdGroups[ id=" + id + " ]";
    }
    
}
