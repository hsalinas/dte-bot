/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "sga_users")
@NamedQueries({
    @NamedQuery(name = "SgaUsers.findAll", query = "SELECT s FROM SgaUsers s")})
public class SgaUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @Column(name = "password_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passwordModified;
    @Basic(optional = false)
    @Column(name = "password_expired")
    private boolean passwordExpired;
    @Basic(optional = false)
    @Column(name = "nombres")
    private String nombres;
    @Basic(optional = false)
    @Column(name = "paterno")
    private String paterno;
    @Basic(optional = false)
    @Column(name = "materno")
    private String materno;
    @Basic(optional = false)
    @Column(name = "rut")
    private String rut;
    @Basic(optional = false)
    @Column(name = "full_name")
    private String fullName;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "serie")
    private String serie;
    @Basic(optional = false)
    @Column(name = "activo")
    private int activo;
    @Basic(optional = false)
    @Column(name = "view")
    private int view;
    @Basic(optional = false)
    @Column(name = "redirect")
    private int redirect;
    @Basic(optional = false)
    @Column(name = "system_sga")
    private boolean systemSga;
    @Basic(optional = false)
    @Column(name = "system_contable")
    private boolean systemContable;
    @Basic(optional = false)
    @Column(name = "system_dte")
    private boolean systemDte;
    @Basic(optional = false)
    @Column(name = "system_dte_documentos")
    private boolean systemDteDocumentos;
    @Basic(optional = false)
    @Column(name = "system_sgd")
    private boolean systemSgd;
    @Basic(optional = false)
    @Column(name = "system_acepta")
    private boolean systemAcepta;
    @Basic(optional = false)
    @Column(name = "system_ofpi")
    private boolean systemOfpi;
    @Basic(optional = false)
    @Column(name = "system_pagos")
    private boolean systemPagos;
    @Basic(optional = false)
    @Column(name = "system_anexos")
    private boolean systemAnexos;
    @Basic(optional = false)
    @Column(name = "system_acceso")
    private boolean systemAcceso;
    @Basic(optional = false)
    @Column(name = "system_bodega")
    private boolean systemBodega;
    @Column(name = "dte_group_id_old")
    private Integer dteGroupIdOld;
    @Basic(optional = false)
    @Column(name = "dte_documento_group_id_old")
    private int dteDocumentoGroupIdOld;
    @Basic(optional = false)
    @Column(name = "sgd_group_id_old")
    private int sgdGroupIdOld;
    @Basic(optional = false)
    @Column(name = "sgd_dependencia_id_old")
    private int sgdDependenciaIdOld;
    @Basic(optional = false)
    @Column(name = "sgd_memo_gobernador")
    private boolean sgdMemoGobernador;
    @Basic(optional = false)
    @Column(name = "etoken")
    private boolean etoken;
    @Basic(optional = false)
    @Column(name = "virtual_user")
    private boolean virtualUser;
    @Basic(optional = false)
    @Column(name = "shutdown")
    private boolean shutdown;
    @Basic(optional = false)
    @Column(name = "sgd_submanager")
    private boolean sgdSubmanager;
    @Basic(optional = false)
    @Column(name = "manager")
    private boolean manager;
    @ManyToMany(mappedBy = "sgaUsersSet", fetch = FetchType.LAZY)
    private Set<SgdGroups> sgdGroupsSet;
    @OneToMany(mappedBy = "sgaUsers", fetch = FetchType.LAZY)
    private Set<SgaGroups> sgaGroupsSet;
    @JoinColumn(name = "cargo_firma_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CargosFirmas cargosFirmas;
    @JoinColumn(name = "sga_departamento_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaDepartamentos sgaDepartamentos;
    @JoinColumn(name = "sga_divisione_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaDivisiones sgaDivisiones;
    @JoinColumn(name = "dte_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DtesGroups dtesGroups;
    @JoinColumn(name = "dte_documento_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DtesDocumentosGroups dtesDocumentosGroups;
    @JoinColumn(name = "tipo_user_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoUsers tipoUsers;
    @JoinColumn(name = "sga_estamentos_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaEstamentos sgaEstamentos;
    @JoinColumn(name = "sgd_dependencia_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgdDependencias sgdDependencias;
    @JoinColumn(name = "centro_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Centros centros;
    @JoinColumn(name = "sgd_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgdGroups sgdGroups;
    @JoinColumn(name = "acepta_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaGroup aceptaGroup;
    @JoinColumn(name = "ofp_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private OfpGroups ofpGroups;
    @JoinColumn(name = "cargo_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cargos cargos;
    @JoinColumn(name = "sga_unidade_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaUnidades sgaUnidades;
    @JoinColumn(name = "sga_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SgaGroups sgaGroups;
    @OneToMany(mappedBy = "sgaUsers", fetch = FetchType.LAZY)
    private Set<Solicitudes> solicitudesSet;
    @OneToMany(mappedBy = "sgaUsers", fetch = FetchType.LAZY)
    private Set<Analistas> analistasSet;
    @OneToMany(mappedBy = "sgaUsers", fetch = FetchType.LAZY)
    private Set<AceptaGroup> aceptaGroupSet;

    public SgaUsers() {
    }

    public SgaUsers(Integer id) {
        this.id = id;
    }

    public SgaUsers(Integer id, Date created, Date modified, String username, String password, Date passwordModified, boolean passwordExpired, String nombres, String paterno, String materno, String rut, String fullName, String email, String serie, int activo, int view, int redirect, boolean systemSga, boolean systemContable, boolean systemDte, boolean systemDteDocumentos, boolean systemSgd, boolean systemAcepta, boolean systemOfpi, boolean systemPagos, boolean systemAnexos, boolean systemAcceso, boolean systemBodega, int dteDocumentoGroupIdOld, int sgdGroupIdOld, int sgdDependenciaIdOld, boolean sgdMemoGobernador, boolean etoken, boolean virtualUser, boolean shutdown, boolean sgdSubmanager, boolean manager) {
        this.id = id;
        this.created = created;
        this.modified = modified;
        this.username = username;
        this.password = password;
        this.passwordModified = passwordModified;
        this.passwordExpired = passwordExpired;
        this.nombres = nombres;
        this.paterno = paterno;
        this.materno = materno;
        this.rut = rut;
        this.fullName = fullName;
        this.email = email;
        this.serie = serie;
        this.activo = activo;
        this.view = view;
        this.redirect = redirect;
        this.systemSga = systemSga;
        this.systemContable = systemContable;
        this.systemDte = systemDte;
        this.systemDteDocumentos = systemDteDocumentos;
        this.systemSgd = systemSgd;
        this.systemAcepta = systemAcepta;
        this.systemOfpi = systemOfpi;
        this.systemPagos = systemPagos;
        this.systemAnexos = systemAnexos;
        this.systemAcceso = systemAcceso;
        this.systemBodega = systemBodega;
        this.dteDocumentoGroupIdOld = dteDocumentoGroupIdOld;
        this.sgdGroupIdOld = sgdGroupIdOld;
        this.sgdDependenciaIdOld = sgdDependenciaIdOld;
        this.sgdMemoGobernador = sgdMemoGobernador;
        this.etoken = etoken;
        this.virtualUser = virtualUser;
        this.shutdown = shutdown;
        this.sgdSubmanager = sgdSubmanager;
        this.manager = manager;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getPasswordModified() {
        return passwordModified;
    }

    public void setPasswordModified(Date passwordModified) {
        this.passwordModified = passwordModified;
    }

    public boolean getPasswordExpired() {
        return passwordExpired;
    }

    public void setPasswordExpired(boolean passwordExpired) {
        this.passwordExpired = passwordExpired;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getRedirect() {
        return redirect;
    }

    public void setRedirect(int redirect) {
        this.redirect = redirect;
    }

    public boolean getSystemSga() {
        return systemSga;
    }

    public void setSystemSga(boolean systemSga) {
        this.systemSga = systemSga;
    }

    public boolean getSystemContable() {
        return systemContable;
    }

    public void setSystemContable(boolean systemContable) {
        this.systemContable = systemContable;
    }

    public boolean getSystemDte() {
        return systemDte;
    }

    public void setSystemDte(boolean systemDte) {
        this.systemDte = systemDte;
    }

    public boolean getSystemDteDocumentos() {
        return systemDteDocumentos;
    }

    public void setSystemDteDocumentos(boolean systemDteDocumentos) {
        this.systemDteDocumentos = systemDteDocumentos;
    }

    public boolean getSystemSgd() {
        return systemSgd;
    }

    public void setSystemSgd(boolean systemSgd) {
        this.systemSgd = systemSgd;
    }

    public boolean getSystemAcepta() {
        return systemAcepta;
    }

    public void setSystemAcepta(boolean systemAcepta) {
        this.systemAcepta = systemAcepta;
    }

    public boolean getSystemOfpi() {
        return systemOfpi;
    }

    public void setSystemOfpi(boolean systemOfpi) {
        this.systemOfpi = systemOfpi;
    }

    public boolean getSystemPagos() {
        return systemPagos;
    }

    public void setSystemPagos(boolean systemPagos) {
        this.systemPagos = systemPagos;
    }

    public boolean getSystemAnexos() {
        return systemAnexos;
    }

    public void setSystemAnexos(boolean systemAnexos) {
        this.systemAnexos = systemAnexos;
    }

    public boolean getSystemAcceso() {
        return systemAcceso;
    }

    public void setSystemAcceso(boolean systemAcceso) {
        this.systemAcceso = systemAcceso;
    }

    public boolean getSystemBodega() {
        return systemBodega;
    }

    public void setSystemBodega(boolean systemBodega) {
        this.systemBodega = systemBodega;
    }

    public Integer getDteGroupIdOld() {
        return dteGroupIdOld;
    }

    public void setDteGroupIdOld(Integer dteGroupIdOld) {
        this.dteGroupIdOld = dteGroupIdOld;
    }

    public int getDteDocumentoGroupIdOld() {
        return dteDocumentoGroupIdOld;
    }

    public void setDteDocumentoGroupIdOld(int dteDocumentoGroupIdOld) {
        this.dteDocumentoGroupIdOld = dteDocumentoGroupIdOld;
    }

    public int getSgdGroupIdOld() {
        return sgdGroupIdOld;
    }

    public void setSgdGroupIdOld(int sgdGroupIdOld) {
        this.sgdGroupIdOld = sgdGroupIdOld;
    }

    public int getSgdDependenciaIdOld() {
        return sgdDependenciaIdOld;
    }

    public void setSgdDependenciaIdOld(int sgdDependenciaIdOld) {
        this.sgdDependenciaIdOld = sgdDependenciaIdOld;
    }

    public boolean getSgdMemoGobernador() {
        return sgdMemoGobernador;
    }

    public void setSgdMemoGobernador(boolean sgdMemoGobernador) {
        this.sgdMemoGobernador = sgdMemoGobernador;
    }

    public boolean getEtoken() {
        return etoken;
    }

    public void setEtoken(boolean etoken) {
        this.etoken = etoken;
    }

    public boolean getVirtualUser() {
        return virtualUser;
    }

    public void setVirtualUser(boolean virtualUser) {
        this.virtualUser = virtualUser;
    }

    public boolean getShutdown() {
        return shutdown;
    }

    public void setShutdown(boolean shutdown) {
        this.shutdown = shutdown;
    }

    public boolean getSgdSubmanager() {
        return sgdSubmanager;
    }

    public void setSgdSubmanager(boolean sgdSubmanager) {
        this.sgdSubmanager = sgdSubmanager;
    }

    public boolean getManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }

    public Set<SgdGroups> getSgdGroupsSet() {
        return sgdGroupsSet;
    }

    public void setSgdGroupsSet(Set<SgdGroups> sgdGroupsSet) {
        this.sgdGroupsSet = sgdGroupsSet;
    }

    public Set<SgaGroups> getSgaGroupsSet() {
        return sgaGroupsSet;
    }

    public void setSgaGroupsSet(Set<SgaGroups> sgaGroupsSet) {
        this.sgaGroupsSet = sgaGroupsSet;
    }

    public CargosFirmas getCargosFirmas() {
        return cargosFirmas;
    }

    public void setCargosFirmas(CargosFirmas cargosFirmas) {
        this.cargosFirmas = cargosFirmas;
    }

    public SgaDepartamentos getSgaDepartamentos() {
        return sgaDepartamentos;
    }

    public void setSgaDepartamentos(SgaDepartamentos sgaDepartamentos) {
        this.sgaDepartamentos = sgaDepartamentos;
    }

    public SgaDivisiones getSgaDivisiones() {
        return sgaDivisiones;
    }

    public void setSgaDivisiones(SgaDivisiones sgaDivisiones) {
        this.sgaDivisiones = sgaDivisiones;
    }

    public DtesGroups getDtesGroups() {
        return dtesGroups;
    }

    public void setDtesGroups(DtesGroups dtesGroups) {
        this.dtesGroups = dtesGroups;
    }

    public DtesDocumentosGroups getDtesDocumentosGroups() {
        return dtesDocumentosGroups;
    }

    public void setDtesDocumentosGroups(DtesDocumentosGroups dtesDocumentosGroups) {
        this.dtesDocumentosGroups = dtesDocumentosGroups;
    }

    public TipoUsers getTipoUsers() {
        return tipoUsers;
    }

    public void setTipoUsers(TipoUsers tipoUsers) {
        this.tipoUsers = tipoUsers;
    }

    public SgaEstamentos getSgaEstamentos() {
        return sgaEstamentos;
    }

    public void setSgaEstamentos(SgaEstamentos sgaEstamentos) {
        this.sgaEstamentos = sgaEstamentos;
    }

    public SgdDependencias getSgdDependencias() {
        return sgdDependencias;
    }

    public void setSgdDependencias(SgdDependencias sgdDependencias) {
        this.sgdDependencias = sgdDependencias;
    }

    public Centros getCentros() {
        return centros;
    }

    public void setCentros(Centros centros) {
        this.centros = centros;
    }

    public SgdGroups getSgdGroups() {
        return sgdGroups;
    }

    public void setSgdGroups(SgdGroups sgdGroups) {
        this.sgdGroups = sgdGroups;
    }

    public AceptaGroup getAceptaGroup() {
        return aceptaGroup;
    }

    public void setAceptaGroup(AceptaGroup aceptaGroup) {
        this.aceptaGroup = aceptaGroup;
    }

    public OfpGroups getOfpGroups() {
        return ofpGroups;
    }

    public void setOfpGroups(OfpGroups ofpGroups) {
        this.ofpGroups = ofpGroups;
    }

    public Cargos getCargos() {
        return cargos;
    }

    public void setCargos(Cargos cargos) {
        this.cargos = cargos;
    }

    public SgaUnidades getSgaUnidades() {
        return sgaUnidades;
    }

    public void setSgaUnidades(SgaUnidades sgaUnidades) {
        this.sgaUnidades = sgaUnidades;
    }

    public SgaGroups getSgaGroups() {
        return sgaGroups;
    }

    public void setSgaGroups(SgaGroups sgaGroups) {
        this.sgaGroups = sgaGroups;
    }

    public Set<Solicitudes> getSolicitudesSet() {
        return solicitudesSet;
    }

    public void setSolicitudesSet(Set<Solicitudes> solicitudesSet) {
        this.solicitudesSet = solicitudesSet;
    }

    public Set<Analistas> getAnalistasSet() {
        return analistasSet;
    }

    public void setAnalistasSet(Set<Analistas> analistasSet) {
        this.analistasSet = analistasSet;
    }

    public Set<AceptaGroup> getAceptaGroupSet() {
        return aceptaGroupSet;
    }

    public void setAceptaGroupSet(Set<AceptaGroup> aceptaGroupSet) {
        this.aceptaGroupSet = aceptaGroupSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SgaUsers)) {
            return false;
        }
        SgaUsers other = (SgaUsers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.SgaUsers[ id=" + id + " ]";
    }
    
}
