/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "dtes_flows")
@NamedQueries({
    @NamedQuery(name = "DtesFlows.findAll", query = "SELECT d FROM DtesFlows d")})
public class DtesFlows implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "old_id")
    private int oldId;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "regla")
    private int regla;
    @Basic(optional = false)
    @Column(name = "old_regla")
    private int oldRegla;
    @Basic(optional = false)
    @Column(name = "is_first")
    private boolean isFirst;
    @Basic(optional = false)
    @Column(name = "is_last")
    private boolean isLast;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @JoinColumn(name = "programa_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Programas programas;
    @JoinColumn(name = "dte_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private DtesGroups dtesGroups;
    @OneToMany(mappedBy = "dtesFlows", fetch = FetchType.LAZY)
    private Set<AceptaFactura> aceptaFacturaSet;

    public DtesFlows() {
    }

    public DtesFlows(Integer id) {
        this.id = id;
    }

    public DtesFlows(Integer id, int oldId, String name, Date created, Date modified, int regla, int oldRegla, boolean isFirst, boolean isLast, boolean enabled) {
        this.id = id;
        this.oldId = oldId;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.regla = regla;
        this.oldRegla = oldRegla;
        this.isFirst = isFirst;
        this.isLast = isLast;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getOldId() {
        return oldId;
    }

    public void setOldId(int oldId) {
        this.oldId = oldId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public int getRegla() {
        return regla;
    }

    public void setRegla(int regla) {
        this.regla = regla;
    }

    public int getOldRegla() {
        return oldRegla;
    }

    public void setOldRegla(int oldRegla) {
        this.oldRegla = oldRegla;
    }

    public boolean getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }

    public boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Programas getProgramas() {
        return programas;
    }

    public void setProgramas(Programas programas) {
        this.programas = programas;
    }

    public DtesGroups getDtesGroups() {
        return dtesGroups;
    }

    public void setDtesGroups(DtesGroups dtesGroups) {
        this.dtesGroups = dtesGroups;
    }

    public Set<AceptaFactura> getAceptaFacturaSet() {
        return aceptaFacturaSet;
    }

    public void setAceptaFacturaSet(Set<AceptaFactura> aceptaFacturaSet) {
        this.aceptaFacturaSet = aceptaFacturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DtesFlows)) {
            return false;
        }
        DtesFlows other = (DtesFlows) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.DtesFlows[ id=" + id + " ]";
    }
    
}
