/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "acepta_flow")
@NamedQueries({
    @NamedQuery(name = "AceptaFlow.findAll", query = "SELECT a FROM AceptaFlow a")})
public class AceptaFlow implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Basic(optional = false)
    @Column(name = "regla")
    private int regla;
    @Basic(optional = false)
    @Column(name = "is_first")
    private boolean isFirst;
    @Basic(optional = false)
    @Column(name = "is_last")
    private boolean isLast;
    @Basic(optional = false)
    @Column(name = "enabled")
    private boolean enabled;
    @JoinColumn(name = "acepta_group_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AceptaGroup aceptaGroup;
    @OneToMany(mappedBy = "aceptaFlow", fetch = FetchType.LAZY)
    private Set<AceptaFactura> aceptaFacturaSet;

    public AceptaFlow() {
    }

    public AceptaFlow(Integer id) {
        this.id = id;
    }

    public AceptaFlow(Integer id, String name, Date created, Date modified, int regla, boolean isFirst, boolean isLast, boolean enabled) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.regla = regla;
        this.isFirst = isFirst;
        this.isLast = isLast;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public int getRegla() {
        return regla;
    }

    public void setRegla(int regla) {
        this.regla = regla;
    }

    public boolean getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }

    public boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public AceptaGroup getAceptaGroup() {
        return aceptaGroup;
    }

    public void setAceptaGroup(AceptaGroup aceptaGroup) {
        this.aceptaGroup = aceptaGroup;
    }

    public Set<AceptaFactura> getAceptaFacturaSet() {
        return aceptaFacturaSet;
    }

    public void setAceptaFacturaSet(Set<AceptaFactura> aceptaFacturaSet) {
        this.aceptaFacturaSet = aceptaFacturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AceptaFlow)) {
            return false;
        }
        AceptaFlow other = (AceptaFlow) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.AceptaFlow[ id=" + id + " ]";
    }
    
}
