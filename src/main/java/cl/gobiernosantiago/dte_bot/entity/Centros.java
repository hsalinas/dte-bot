/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gobiernosantiago.dte_bot.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author hsalinas
 */
@Entity
@Table(name = "centros")
@NamedQueries({
    @NamedQuery(name = "Centros.findAll", query = "SELECT c FROM Centros c")})
public class Centros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "sga_divisione_id")
    private int sgaDivisioneId;
    @Basic(optional = false)
    @Column(name = "sga_departamento_id")
    private int sgaDepartamentoId;
    @Basic(optional = false)
    @Column(name = "sga_unidade_id")
    private int sgaUnidadeId;
    @Basic(optional = false)
    @Column(name = "sga_user_id")
    private int sgaUserId;
    @OneToMany(mappedBy = "centros", fetch = FetchType.LAZY)
    private Set<SgaUsers> sgaUsersSet;

    public Centros() {
    }

    public Centros(Integer id) {
        this.id = id;
    }

    public Centros(Integer id, int sgaDivisioneId, int sgaDepartamentoId, int sgaUnidadeId, int sgaUserId) {
        this.id = id;
        this.sgaDivisioneId = sgaDivisioneId;
        this.sgaDepartamentoId = sgaDepartamentoId;
        this.sgaUnidadeId = sgaUnidadeId;
        this.sgaUserId = sgaUserId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getSgaDivisioneId() {
        return sgaDivisioneId;
    }

    public void setSgaDivisioneId(int sgaDivisioneId) {
        this.sgaDivisioneId = sgaDivisioneId;
    }

    public int getSgaDepartamentoId() {
        return sgaDepartamentoId;
    }

    public void setSgaDepartamentoId(int sgaDepartamentoId) {
        this.sgaDepartamentoId = sgaDepartamentoId;
    }

    public int getSgaUnidadeId() {
        return sgaUnidadeId;
    }

    public void setSgaUnidadeId(int sgaUnidadeId) {
        this.sgaUnidadeId = sgaUnidadeId;
    }

    public int getSgaUserId() {
        return sgaUserId;
    }

    public void setSgaUserId(int sgaUserId) {
        this.sgaUserId = sgaUserId;
    }

    public Set<SgaUsers> getSgaUsersSet() {
        return sgaUsersSet;
    }

    public void setSgaUsersSet(Set<SgaUsers> sgaUsersSet) {
        this.sgaUsersSet = sgaUsersSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Centros)) {
            return false;
        }
        Centros other = (Centros) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.gobiernosantiago.dte_bot.entity.Centros[ id=" + id + " ]";
    }
    
}
